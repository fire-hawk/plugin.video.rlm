#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import json


website_url = 'https://www.redlettermedia.com'
review_shows = [
	{'name': 'Half in the Bag', 'url': '/half-in-the-bag'},
	{'name': 'Best of the Worst', 'url': '/best-of-the-worst'},
	{'name': 're:View', 'url': '/review'},
	{'name': 'Plinkett Reviews', 'url': '/plinkett-reviews'}
]
web_videos_and_shorts = [
	# {'name': 'The Nerd Crew', 'url': '/the-nerd-crew'},
	{'name': 'Movie Talk', 'url': '/movie-talk'},
	{'name': 'Behind the Scenes', 'url': '/behind-the-scenes'},
	{'name': 'Shorts Films/Random', 'url': '/shorts-filmsrandom'}
]

categories = [
  {'name': 'REVIEW SHOWS', 'shows': review_shows},
  {'name': 'WEB VIDEOS AND SHORTS', 'shows': web_videos_and_shorts}
]


def getIndexPage(web_url):
	"""
	returns a list of video entries and a link to older entries
	"""
	webpage = requests.get(web_url)
	soup = BeautifulSoup(webpage.content, "html.parser")
	page = {
		'entries': [],
		'older_posts': None
	}
	els = soup.find_all(class_='entry')
	for el in els:
		entry = {}
		entry['name'] = el.h1.text.strip()
		entry['link'] = el.a['href']
		entry['thumbnail'] = el.img['data-src']
		page['entries'].append(entry)
	if soup.find(class_='older').a is not None:
		page['older_posts'] = soup.find(class_='older').a['href']
	else:
		page['older_posts'] = None
	return page


def getVideoPage(web_url):
	"""
	parses a video page and returns the 'youtube video id'
	"""
	webpage = requests.get(web_url)
	soup = BeautifulSoup(webpage.content, "html.parser")
	json_data = json.loads(soup.find(class_='video-block')['data-block-json'])
	return json_data['url'].split('/')[3]


if __name__ == '__main__':
  pass

