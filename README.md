It parses the redlettermedia.com website.

It's very basic, but it works, except for 'The Nerd Crew' for some reason.

I think the HTML code on the website differs.

# Menu Tree

- REVIEW SHOWS
  - Half in the Bag
  - Best of the Worst
  - re:View
  - Plinket Reviews
- WEB VIDEOS AND SHORTS
  - ~~The Nerd Crew~~
  - Movie Talk
  - Behind the Scenes
  - Shorts Films/Random

# Installation

1. Download Source Code as `.zip`
2. Kodi > Addons > Install from zip
