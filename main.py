# Module: main
# Author: fire-hawk
# Created on: 08.10.2021
# License: GPL
"""
Unofficial "Red Letter Media" Addon
"""
import sys
from urllib.parse import urlencode, parse_qsl
import xbmcgui
import xbmcplugin

import rlm

# Get the plugin url in plugin:// notation.
_URL = sys.argv[0]
# Get the plugin handle as an integer number.
_HANDLE = int(sys.argv[1])


def list_categories():
  for category in rlm.categories:
    list_item = xbmcgui.ListItem(label=category['name'])
    is_folder = True
    xbmcplugin.addDirectoryItem(_HANDLE, 'plugin://plugin.video.rlm/?action=list_shows&category=%s' % category['name'], list_item, is_folder)
  xbmcplugin.endOfDirectory(_HANDLE)


def list_shows(category):
  if category == 'REVIEW SHOWS':
    shows = rlm.review_shows
  elif category == 'WEB VIDEOS AND SHORTS':
    shows = rlm.web_videos_and_shorts

  for show in shows:
    list_item = xbmcgui.ListItem(label=show['name'])
    is_folder = True
    xbmcplugin.addDirectoryItem(_HANDLE, 'plugin://plugin.video.rlm/?action=list_videos&show=%s' % show['name'], list_item, is_folder)
  xbmcplugin.endOfDirectory(_HANDLE)


def list_videos(show_url):

  page = rlm.getIndexPage(rlm.website_url + show_url)
  
  xbmcplugin.setContent(_HANDLE, 'videos')
  
  for entry in page['entries']:
    list_item = xbmcgui.ListItem(label=entry['name'])
    list_item.setInfo('video', {'title': entry['name'],
                      'mediatype': 'video'})
    list_item.setArt({'thumb': entry['thumbnail'], 'icon': entry['thumbnail'], 'fanart': entry['thumbnail']})
    is_folder = False
    xbmcplugin.addDirectoryItem(_HANDLE, 'plugin://plugin.video.rlm/?action=play_video&url=%s' % entry['link'], list_item, is_folder)
  
  if page['older_posts'] is not None:
    list_item = xbmcgui.ListItem(label='Older Posts >')
    is_folder = True
    xbmcplugin.addDirectoryItem(_HANDLE, 'plugin://plugin.video.rlm/?action=list_videos&url=%s' % page['older_posts'], list_item, is_folder)
  
  xbmcplugin.endOfDirectory(_HANDLE)


def play_video(video_url):
  ytid = rlm.getVideoPage(rlm.website_url + video_url)
  strm_url = 'plugin://plugin.video.youtube/play/?video_id=%s'% ytid
  xbmc.Player().play(strm_url)


if __name__ == '__main__':
  paramstring = sys.argv[2][1:]
  params = dict(parse_qsl(paramstring))

  if params:
    if params['action'] == 'list_categories':
      list_categories()

    elif params['action'] == 'list_shows':
      list_shows(params['category'])

    elif params['action'] == 'list_videos':
      
      if 'show' in params:
        for show in rlm.review_shows:
          if show['name'] == params['show']:
            show_url = show['url']

        for show in rlm.web_videos_and_shorts:
          if show['name'] == params['show']:
            show_url = show['url']
      else:
        show_url = params['url']

      list_videos(show_url)

    elif params['action'] == 'play_video':
      # Play a video from a provided URL.
      play_video(params['url'])
    else:
      # If the provided paramstring does not contain a supported action
      # we raise an exception. This helps to catch coding errors,
      # e.g. typos in action names.
      raise ValueError('Invalid paramstring: {}!'.format(paramstring))
  else:
    # If the plugin is called from Kodi UI without any parameters,
    # display the categories
    list_categories()
